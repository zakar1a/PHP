<?php
    
    $test_var = 8.23;
    $br = "<br>";
    echo "Changing Data Type By Casting " . $br . $br;
    
    echo "The original value is: ";
    echo $test_var . $br;
    echo "Casting to String: ";
    echo (string) $test_var . $br;
    echo "Casting to Integer: ";
    echo (int) $test_var . $br;
    echo "Casting to Float: ";
    echo (float) $test_var . $br;
    echo "Casting to Boolean: ";
    echo (boolean) $test_var . $br . $br;
    
    echo "Other ways are using intval(value) floatval (value) strval (value)";


?>