<?php

    $test_var = 8.23;                               //Initial value
    $br = "<br>";                                   //Return to line in a variable
    echo "Original value is:";                      // Making things more human readable
    echo $test_var . $br;                           //First test to print 8.23

    echo "--------------------------------" . $br;  //Making a space
    
    $test_var = 8.23;
    echo "original value is:" . $test_var . $br;    // Printing original value
    echo "The data type is:";                       // Making things more humean readable
    settype($test_var, "string");                   // Setting the type as string
    echo gettype($test_var) . $br;                  // Printing the new data type
    echo "the new value is:";                       // Making things more human readable
    echo $test_var . $br;                           // Printing the test_var
  
    
    echo "--------------------------------" . $br;  //Making a space
    
    $test_var = 8.23;                               // Resetting to the original value
    echo "original value is:" . $test_var . $br;    // Printing original value
    echo "The data type is:";                       // Making things more humean readable
    settype($test_var, "boolean");                  // Setting new value to Boolean
    echo gettype ($test_var) . $br;                 // Printing the data type
    echo "the new value is:";                       // Making things more human readable
    echo $test_var . $br;                           // Printing Result = 1 since boolean is either 0 or 1

    echo "--------------------------------" . $br;  //Making a space

    $test_var = 8.23;                               // Resetting the value to 8.23
    echo "Original value is:" . $test_var . $br;    // Making things more humean readable
    echo "The data type is:";                       // Making things more humean readable
    settype($test_var, "float");                    // Setting new value to float
    echo gettype($test_var) . $br;                  // Making things more human readable
    echo "the new value is:";                       // Making things more human readable
    echo $test_var . $br;                           // Printing Result

    echo "--------------------------------" . $br;  //Making a space
    
    $test_var = 8.23;                               // Resetting to the original value
    echo "original value is:" . $test_var . $br;    // Printing original value
    echo "The data type is:";                       // Making things more humean readable
    settype($test_var, "integer");                  // Setting new value to Integer
    echo gettype ($test_var) . $br;                 // Printing the data type
    echo "the new value is:";                       // Making things more human readable
    echo $test_var . $br;                           // Printing Result 


?>