<?php

    echo "Write a Script that creates a variable and assigns an integer value to it, then adds 1 to the variable's value three times, using a different operator each time. Display the final result to the user" . "<br>";

    $X = 5;
    $X = $X + 1;
    $X += 1;
    $X++;
    echo "Result is: ";
    echo $X . "<br>";


    echo "Write a Script that creates two variables and assigns a different integer value to each variable. Now make your script test whether the first value is" . "<br>";
    
    echo "A. equal to the second value" . "<br>";
    echo "B. Greater than the second value" . "<br>";
    echo "C. Less than or equal to the second value" . "<br>";
    echo "D. Not equal to the second value" . "<br>";
    echo "and output the result of each test to the user" . "<br>" . "<br>";
    
    echo "My Way to do this section of exercise:" . "<br>";

    $Num1   = 5;
    $Num2   = 2;

    echo "A: ";
    if ($Num1 == $Num2) {
        echo "True"; }
            else {
                echo "False";}
    echo "<br>";

    echo "B: ";
    if ($Num1 > $Num2) {
        echo "True";
    } else {
        echo "False";
    }
    
    echo "<br>";
    echo "C: ";
    if ($Num1 <= $Num2){
        echo "True";
    }   else{
        echo "False";
    }
    
    echo "<br>";
    echo "D: ";
    if ($Num1 != $Num2){
        echo "True";
    } else{
        echo "False";
    }


    echo "<br>" . "<br>";
    echo "Solution from the book" . "<br>" . "<br>";

    echo "Test 1 result: " . ($Num1 == $Num2) . "<br>";
    echo "Test 2 result: " . ($Num1 > $Num2) .  "<br>";
    echo "Test 3 result: " . ($Num1 <= $Num2) . "<br>";
    echo "Test 4 result: " . ($Num1 != $Num2) . "<br>";

    echo "<br>";
    echo "NOTE: 1 means True, Empty result means False";

           
    
    



    