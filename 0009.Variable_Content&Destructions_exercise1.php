<?php   
 
$name='Zakaria'; 
$age=28;   
 
var_dump ($name); 
echo "<br/>";   
 
print_r  ($name); 
echo "<br/>";   
 
var_dump  ($age); 
echo "<br/>";  
 
$name=null;
//In PHP versions up to 5.3.3, this statement could also have 
//been written: unset($name);, and the next command would work correctly. 
//More recent versions return an undefined variable error 
//for var_dump after the variable is unset.
 
var_dump  ($name);
//The last two statements would have worked just as well with the $age variable.
 
?> 