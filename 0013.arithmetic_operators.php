<html>
<title> Arithmetic Operators </title>
<body>
<?php
	// This is an addition operation
	$X = 10;
	$Y = 15;
	$Z = 19;

	$Result = ($X + $Y + $Z);
	echo "This is an addition Operation" . "<br>";
	echo "X = 10 / Y = 15" . "<br>";
	echo $Result . "<br>"; 

	// This is a Substraction operation

	$A = 9;
	$B = 1;
	$C = $A - $B;
	echo "This is a Substraction Operation" . "<br>";
	echo "X = 9 / Y = 1" . "<br>";
	echo $C . "<br>";

	// This is a Multiplication Operation

	$D = 5;
	$E = 4;
	$F = $D * $E;
	echo "This is a Multiplication Operation" . "<br>";
	echo "X = 5 / Y = 4" . "<br>";
	echo $F . "<br>";

	//This is a Division Operation

	$G = 214;
	$H = 3;
	$I = $G / $H;
	echo "This is a Division Operation" . "<br>";
	echo "X = 214 / Y = 3" . "<br>";
	echo $I . "<br>";

	//This is a Modulus Operation (Remainder of $J / $K)

	$J = 16;
	$K = 8;
	$L = $J % $K;
	echo "This is a Modulus Operation [Remainder of 2 numbers]" . "<br>";
	echo "X = 16 / Y = 8" . "<br>";
	echo $L . "<br>";

	//This is an Exponentiation operation ($M to the $N th power)

	$M = 2;
	$N = 5;
	$O = $M ** $N;
	echo "This is an Exponentiation Operation [Number 1 to the power of Number 2]" . "<br>";
	echo "X = 2 / Y = 5" . "<br>";
	echo $O . "<br>";

	//Negative number

	$P = 5;
	$Q = 7;
	$S = $P - $Q;
	echo "Negative number based on Substraction" . "<br>";
	echo "X = 5 / Y = 7" . "<br>";
	echo $S . "<br>";

?>
</body>
</html>