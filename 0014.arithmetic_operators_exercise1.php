<?php 

/*
PHP includes all the standard arithmetic operators. For this PHP exercise, you will use them along with variables to print equations to the browser. In your script, create the following variables:
$x=10;
$y=7;
Write code to print out the following:
10 + 7 = 17
10 - 7 = 3
10 * 7 = 70
10 / 7 = 1.4285714285714
10 % 7 = 3
Use numbers only in the above variable assignments, not in the echo statements. You will need a third variable as well.
Note: this is intended as a simple, beginning exercise, not using arrays or loops. Some of the solutions in comments include these structures. If you don't understand them, just keep learning, and you will.
 */


$x = 10;
$y = 7;
$addition = $x + $y;
$subst = $x - $y;
$mult = $x * $y;
$division = $x / $y;
$exp = $x % $y;

echo "10 + 7 =";
echo $addition . "<br>"; // addition

echo "10 - 7 =";
echo $subst . "<br>"; // Substraction

echo "10 X 7 =";
echo $mult . "<br>"; // Multiplication

echo "10 / 7 =";
echo $division . "<br>"; // Division

echo "10 % 7 =";
echo $exp . "<br>"; // Exponentiation



 ?>