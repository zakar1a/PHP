<?php

        echo "Comparisons Examples" . "<br>";
        echo "10 > 5 is True [Greather Than]" . "<br>";
        echo "10 < 5 is False [Less than]" . "<br>";
        echo "10 == 5 is False [Equal]" . "<br>";
        echo "10 != 5 is True [Not Equal]" . "<br>";

        echo "<br>";
      
        $x  = 23;
        $br = "<br>";
        
        echo "Initial value is:23 " . $br . $br;

        echo "Checking if 23 <24" . $br;
        echo "Result is: ";
        echo ($x < 24) . " True." . $br;
        echo $br;
    
        echo "Checking if 23 < '24' " . $br;
        echo "Result is: ";
        echo ($x < "24"). " True. PHP converts string to integer" . $br . $br;

        echo "Checking if 23 == 24" . $br;
        echo "Result is: ";
        echo ($x == 23) . " True." . $br . $br;

        echo "Checking if 23 === 23" . $br;
        echo "Result is:";
        echo ($x === 23) . " True. " . $br . $br;
            
        echo "Checking if 23 === '23' " . $br;
        echo "Reuslt is: ";
        echo ($x === "24") . " False. Because 23 and '23' are not the same data type" . $br . $br;
        
        echo "Checking if 23 >= 23" . $br;
        echo "Result is: ";
        echo ($x >= 23) . " True." . $br . $br; 

        echo "Checking if 23 >= 24" . $br;
        echo "Result is: ";
        echo ($x >= 24) . " False."
    
?>