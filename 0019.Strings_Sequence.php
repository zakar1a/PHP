<?php
    //    Here ’ s a list of the more common escape sequences that you can use within double- quoted strings:
    //1.    S equence M eaning
    //2.    \n A line feed character (ASCII 10)
    //3.    \r A carriage return character (ASCII 13)
    //4.    \t A horizontal tab character (ASCII 9)
    //5.    \v A vertical tab character (ASCII 11)
    //6.    \f A form feed character (ASCII 12)
    //7.    \\ A backslash (as opposed to the start of an escape sequence)
    //8.    \$ A $ symbol (as opposed to the start of a variable name)
    //9.    \" A double quote (as opposed to the double quote marking the end of a string)


    //Return to line
    $br = "<br>";

    echo 'It\'s a nice day' . $br;

    echo "This is <span style=\"color:red;\">Red</span> test" . $br;

    // 2

    $MyNewline = "This ends with a line return\n" . $br;
    //echo $MyNewline;

    // 7 

    $MyFilePath = "c:\\windows\\system32\\myfile.txt" . $br;
    echo $MyFilePath;

    // 8

    echo "I have \$money in my pocket" . $br;

    // 9

    $my_string = "\"World\" is beautiful\n \"sky\" is blue\n \"grass\" is green";
    echo $my_string;

    
   

    