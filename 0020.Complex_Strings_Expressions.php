<?php
    $br = "<br>";

    $favourite_animal = "Cat";

    echo "My Favourite animals are {$favourite_animal}s" . $br; // Or also ${favourite_animal}

    $My_Array["age"] = 34;
    echo "My age is {$My_Array["age"]}" . $br; // Using an Array

    // OR

    echo "My age is " . $My_Array["age"] . $br;  //Concatenation



    //==========================================//
   