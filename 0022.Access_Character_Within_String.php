<?php
        $br = "<br>";
        $myString = "Hello, world!";
        echo $myString[0] . $br; // Displays ‘H’
        echo $myString[7] . $br; // Displays ‘w’
        $myString[12] = '?';
        echo $myString . $br; // Displays ‘Hello, world?

        echo $br;

        //To extract a sequence of characters from a string
        //you can use PHP's substr() function

        $myString = "Hello, world!";
        echo substr( $myString, 0, 5 ) . $br; // Displays ‘Hello’
        echo substr($myString, 11) . $br;
