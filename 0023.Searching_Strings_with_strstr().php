<?php

        //If you just want to find out whether some text occurs within a string, use strstr() . 
        //This takes two parameters: the string to search through, and the search text. 
        //If the text is found, strstr() returns the portion of the string from the start 
        //of the found text to the end of the string. If the text isn ’ t found, it returns false . For example:


        $br = "<br>";
        $myString = "Hello, world!";
        echo strstr( $myString, "wor" ) . $br; // Displays ‘world!’
        echo ( strstr( $myString, "AS" ) ? "yes" : "No Existing" ) . $br; // Displays No Existing