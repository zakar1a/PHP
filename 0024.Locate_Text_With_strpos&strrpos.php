<?php
        $br = "<br>";

        $myString = "Hello, world!";
        echo strpos( $myString, "wor" ); // Displays ‘7’
        echo strpos( $myString, "xyz" ); // Displays ‘’ (false) [this will display nothing]

        echo $br;

        $myString2 = "Hello, world!";
        echo strpos( $myString, "o" ) . $br; // Displays ‘4’
        echo strpos( $myString, "o", 5 ) . $br; // Displays ‘8’

        echo $br;

        $myString = "Hello, world!";
        echo strpos( $myString, "o" ) . $br; // Displays ‘4’
        echo strrpos( $myString, "o" ) . $br; // Displays ‘8’ - This will go from the 
                                            //end and will reach the first desired letter from the start to find 
                                            //then will calculate the characters numbers