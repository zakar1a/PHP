<?php

    $br =   "<br>";
    //Occasionally you might need to know how many times some text occurs within a string

    $myString = "I say, nay, nay, and thrice nay!";
    echo substr_count( $myString, "nay" ) . $br;  // Displays ‘3’

    //You can also pass an optional third argument to specify the index position to start searching, and an
    //optional fourth argument to indicate how many characters the function should search before giving up.
    //Here are some examples that use these third and fourth arguments:


    $myString = "I say, nay, nay, and thrice nay!";
    echo substr_count( $myString, "nay", 9 ) . $br; // Displays ‘2’
    echo substr_count( $myString, "nay", 9, 6 ) . $br; // Displays ‘1’

    echo $br;

    $char  = "a,a,a,a";
    echo substr_count($char, "a", 5) . $br; //Count 1 characters then count how many "a" exist after that
    echo substr_count($char, "a", 2, 3) . $br; //Count 2 characters then start counting for 3 characters how many "a" exists after that.