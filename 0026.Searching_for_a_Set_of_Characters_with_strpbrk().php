<?php

        //To find out if a string contains any one of a set of characters
        
        $br =   "<br>";
        $myString = "Hello, world!";
        echo strpbrk( $myString, "abcdefgH" ); // Displays ‘ello, world!’
        echo strpbrk( $myString, "xyz" ); // Displays ‘’ (false) [nothing will be displayed]

        echo $br;

        $username = "matt@example.com";
        if ( strpbrk( $username, "@!" ) ) 
            echo "@ and ! are not allowed in usernames";

        echo $br;
            
        $password = "zxs!";
        if ( strpbrk($password, "!@" ) )
            echo "@ and ! not allowed";
            else{
                echo "chosen password is ok";
            }