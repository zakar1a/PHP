<?php
        $br = "<br>";
        //As well as being able to search for text within a larger string, you can also replace portions of a string
        //with different text. This section discusses three useful PHP functions for replacing text:


        //1. str_replace() replaces all occurrences of the search text within the target string
        //2. substr_replace() replaces a specified portion of the target string with another string
        //3. strtr() replaces certain characters in the target string with other characters

        /////////////////////////////////////////////////////////////////////////////////////////////////


        // 1

        //Replacing All Occurrences using str_replace()
        //str_replace() lets you replace all occurrences of a specified string with a new string. It ’ s the PHP
        //equivalent of using the Replace All option in a word processor


        $myString = "It was the best of times, it was the worst of times,";
        // Displays “It was the best of bananas, it was the worst of bananas,”
        echo str_replace( "times", "bananas", $myString ); //  str_replace( "WordToChange", "NewWord", $From_my_variable );

        echo $br;

        // "worst" will be replaced by "best"

        $string_2 = "It was one of the worst experiences ever";
        echo str_replace("worst", "best", $string_2, $num);

        echo $br;

        // Displays “The text was replaced 2 times.”
        // echo "The text was replaced $num time". $br;
        if ($num > 1){
            echo ("The text was replaced $num times");
            echo $br;
        } else {
            echo ("The text was replaced $num time");
        }

        echo $br;

        ////////////////////////////////////////////////////////////////////////////////////////////////////

        // 2

        // Replacing a Portion of a String with substr_replace()

        //substr_replace() replaces a specific portion of the target string

        $myString = "It was the best of times, it was the worst of times,";
        // Displays “It was the bananas”
        echo substr_replace( $myString, "bananas", 11 ) . $br; //Count 11 char then add bananas and hide the rest ahead
        echo $br;
        //If you don’t want to replace all the text from the start point to the end of the string, you can specify an
        //optional fourth argument containing the number of characters to replace:

        $myString = "It was the best of times, it was the worst of times,";
        // Displays “It was the best of bananas, it was the worst of times,”
        echo substr_replace( $myString, "bananas", 19, 5 ) . $br;
        echo $br;

        //Pass a negative fourth argument to replace up to that many characters from the end of the string:
        $myString = "It was the best of times, it was the worst of times,";
        // Displays “It was the best of bananas the worst of times,”
        echo substr_replace( $myString, "bananas", 19, -20 ) . $br;
        echo $br;
        //You can also pass a zero value to insert the replacement text into the string rather than replacing
        //characters:


        $myString = "It was the best of times, it was the worst of times,";
        // Displays “It really was the best of times, it was the worst of times,”
        echo substr_replace( $myString, "really ", 3, 0 ) . $br;
        echo $br;

        ///////////////////////////////////////////////////////////////////////////////////////////////

        // 3

        //Translating Characters with strtr()

        //to be able to replace certain
        //characters in a string with certain other characters. For example, you might want to make a string “ URL
        //friendly ” by replacing spaces with + (plus) symbols and apostrophes with - (hyphen) symbols.

        $myString = "Her's a little string";
        // Displays “Here-s+a+little+string”
        echo strtr( $myString, " '", "+-" ) . $br;



