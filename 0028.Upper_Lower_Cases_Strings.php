<?php

        $br = "<br>";

        //To convert a string to all lowercase, use strtolower() . 
        //This function takes a string to convert, and
        //returns a converted copy of the string:

        // 1 Lowercase:
        echo "1:";
        $myString = "Hello, world!";
        echo strtolower( $myString ); // Displays ‘hello, world!’
        echo $br;

        // 2 Uppercase:
        echo "2:";
        $myString = "Hello, world!";
        echo strtoupper( $myString ); // Displays ‘HELLO, WORLD!’
        echo $br;

        // 3 ucfirst() makes just the first letter of a string uppercase:
        echo "3:";
        $myString = "hello, world!";
        echo ucfirst( $myString ); // Displays ‘Hello, world!’
        echo $br;

        // 4 lcfirst() – – introduced in PHP 5.3 — makes the first letter of a string lowercase:
        echo "4:";
        $myString = "Hello, World!";
        echo lcfirst( $myString ); // Displays ‘hello, World!'
        echo $br;

        // 5 Finally, ucwords() makes the first letter of each word in a string uppercase:
        echo "5:";
        $myString = "hello, world!";
        echo ucwords( $myString ); // Displays ‘Hello, World!’
        echo $br;