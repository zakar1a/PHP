<?php
    $br = "<br>";
    // Displays “Pi rounded to a whole number is: 3”
    printf( "Pi rounded to a whole number is: %d", M_PI );

    echo $br;

    // Displays “2 times 3 is 6.”
    //“%d”, is called a type specifier

    printf( "%d times %d is %d.", 2, 3, 2*3 );

    echo $br;

    printf("%d plus %d is %d.", 1,1,1+1);

    echo $br;
    echo $br;

    //Example of other type specifiers

    $myNumber = 123.45;
    echo "Initial number is: $myNumber" . $br;
    printf( "Binary: %b" . $br, $myNumber );
    printf( "Character: %c" . $br, $myNumber );
    printf( "Decimal: %d" . $br, $myNumber );
    printf( "Scientific: %e" . $br, $myNumber );
    printf( "Float: %f" . $br, $myNumber );
    printf( "Octal: %o" . $br, $myNumber );
    printf( "String: %s" . $br, $myNumber );
    printf( "Hex (lower case): %x" . $br, $myNumber );
    printf( "Hex (upper case): %X" . $br, $myNumber );

    echo $br;
    echo $br;

    //Specifying Sign
    echo "Specifiying Sign = Minus and Plus" . $br;
    printf( "%d" . $br, 123 ); // Displays “123”
    printf( "%d" . $br, -123 ); // Displays “-123”
    printf( "%+d" . $br, 123 ); // Displays “+123”
    printf( "%+d" . $br, -123 ); // Displays “-123”