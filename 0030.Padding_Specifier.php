<?php

    // Padding with printf

    $br = "<br>";
    //For example, the following code displays various numbers, using leading zeros where necessary to
    //ensure the result is always six digits long 

    printf( "%06d" . $br, 1); // Displays 000001
    printf( "%06d" . $br, 123 ); // Displays “000123”
    printf( "%06d" . $br, 4567 ); // Displays “004567”
    printf( "%06d" . $br, 123456 ); // Displays “123456”
    printf( "%010d" . $br, 10); // Displays 0000010


    echo $br;
    echo $br;

    print "<pre>";
    printf( "% 15s\n", "Hi" ); //Did you notice the space between % and 15?
    printf( "% 15s\n", "Hello" );
    printf( "% 15s\n", "Hello, world!" );
    print "</pre>";
        
    echo $br;
    echo $br;

    print "<pre>";
    printf( "%'@20s\n", "Hi" );
    printf( "%'@20s\n", "Hello" );
    printf( "%'@20s\n", "Hello, world!" );
    print "</pre>";

    echo $br;
    printf( "%'#8s", "Hi" ); // Displays “######Hi”

    echo $br;

    //To display it from right to left

    printf( "%'#-8s", "Hi" ); // Displays “Hi###### [Did you notice a minus there, open your eyes!]
