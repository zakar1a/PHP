<?php
        $br = "<br>";
        echo "original Number is 123.4567";
        echo $br;
        printf( "%f" . $br, 123.4567 ); // Displays “123.456700” (default precision)
        printf( "%.2f" . $br, 123.4567 ); // Displays “123.46”
        printf( "%.0f" . $br, 123.4567 ); // Displays “123”
        printf( "%.10f" . $br, 123.4567 ); // Displays “123.4567000000”

        echo $br;
        echo $br;

        echo "<pre>";
        printf( "%.2f" .$br , 123.4567 ); // Displays “123.46”
        printf( "%012.2f" . $br, 123.4567 ); // Displays “000000123.46”
        printf( "%12.4f" . $br, 123.4567 ); // Displays “ 123.4567”
        echo "</pre>";

        echo $br;
        printf( "%.8s\n", "Hello, world!" ); // Displays “Hello, w