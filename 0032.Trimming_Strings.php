<?php

    //trim() removes white space from the beginning and end of a string
    //ltrim() removes white space only from the beginning of a string
    //rtrim() removes white space only from the end of a string
    
    $myString =  " What a lot of space! ";
    echo "<pre>";
    echo "|" . trim( $myString ) . "|\n"; // Displays “|What a lot of space!|”
    echo "|" . ltrim( $myString ) . "|\n"; // Displays “|What a lot of space! |”;
    echo "|" . rtrim( $myString ) . "|\n"; // Displays “| What a lot of space!|”;
    echo "</pre>";

    $milton1 = "1: The mind is its own place, and in it self\n";
    $milton2 = "2: Can make a Heav'n of Hell, a Hell of Heav'n.\n";
    $milton3 = "3: What matter where, if I be still the same,\n";
    echo "<pre>";
    echo ltrim( $milton1, "0..9: " );
    echo ltrim( $milton2, "0..9: " );
    echo ltrim( $milton3, "0..9: " ); // 0..9 it will search if the first digits is between 0 and 9 and removes it
    echo "</pre>";