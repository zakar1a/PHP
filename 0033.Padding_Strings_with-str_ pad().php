<?php

        echo '<pre>"';
        echo str_pad( "Hello, world!", 20 ); // Displays "Hello, world!       " <== Did you notice the spacing between world! and" ?
        echo '"</pre>';

        //Example using a variable.
        //it will add the left spacing filled by number 1
        $fill = 1;
        echo '<pre>"';
        echo str_pad( "Hello, world!", 20, $fill ); // "Hello, world!1111111"
        echo '"</pre>';

        //Another Example

        // Displays “Hello, world!*******”
        echo "<pre>";
        echo str_pad( "Hello, world!", 20, "*" ) . "\n";
        echo "</pre>";

        //Another Example

        // Displays “Hello, world!1231231”
        echo "<pre>";
        echo str_pad( "Hello, world!", 20, "123" ) . "\n";
        echo "</pre>";

        //Another Example

        // Displays Hello Man!XXXXX
        echo "<pre>";
        echo str_pad( "Hello Man!", 15, "X") . "\n";
        echo "</pre>";

        /*You can also make str_pad() add padding to the left of the string, or to both the left and the right
        of the string. To do this, pass an optional fourth argument comprising one of the following built- in
        constants:
        STR_PAD_RIGHT to pad the string on the right (the default setting), left- aligning the string
        STR_PAD_LEFT to pad the string on the left, right - aligning the string
        STR_PAD_BOTH to pad the string on both the left and the right, centering the result as much
        as possible*/

        //Example

        //The following example adds padding to both the left and right of a string:
        // Displays “***Hello, world!****”
        echo "<pre>";
        echo str_pad( "Hello, world!", 20, "*", STR_PAD_BOTH ) . "\n";
        echo "</pre>";