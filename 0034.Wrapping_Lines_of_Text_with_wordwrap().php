<?php
        $br = "<br>";
        //Wrapping large text

        $myString = "But think not that this famous town has only harpooneers, cannibals, and bumpkins to show her visitors. Not at all. Still New Bedford is a queer place. Had it not been for us whalemen, that tract of land would this day perhaps have been in as howling condition as the coast of Labrador.";
        echo "<pre>";
        echo wordwrap( $myString ); // The default max string output before returning to a new line is 75
        echo "</pre>";

        //To specify the number of char before returning a new line is by specifying a new argument

        echo "<br>";


        $myString_1 = "But think not that this famous town has only harpooneers, cannibals, and bumpkins to show her visitors. Not at all. Still New Bedford is a queer place. Had it not been for us whalemen, that tract of land would this day perhaps have been in as howling condition as the coast of Labrador.";
        echo "<pre>";
        echo wordwrap( $myString_1, 40 ); 
        echo "</pre>";

        echo $br;

        /*You can also pass an optional fourth argument to wordwrap() . If this argument is true (the default is
        false ), the function always wraps the string to the specified line width, even if this means splitting
        words that are longer than the line width. Here ’ s an example:*/

        $myString = "This string has averylongwordindeed.";
        echo wordwrap ( $myString, 10, $br );
        echo $br . $br;
        echo wordwrap ( $myString, 10, $br, true );
        