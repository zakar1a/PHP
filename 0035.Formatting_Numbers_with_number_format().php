<?php
    $br = "<br>";
    echo number_format( 1234567.89 ); // Displays “1,234,568”
    echo $br;
    echo number_format( 1234567.89, 1 ); // Displays “1,234,567.9”
    echo $br;
    echo number_format( 1234567.89, 2, ",", " " ); // Displays “1 234 567,89”
    echo $br;
    echo number_format( 1234567.89, 2, ".", "" ); // Displays “1234567.89”
    echo $br;

    //Money format

    echo number_format("1000000") . $br;
    echo number_format("1000000",2) . $br;
    echo number_format("1000000",2,",",".");