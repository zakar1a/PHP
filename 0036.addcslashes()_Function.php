<?php

//usage addcslashes(string,characters) 

$str = addcslashes("Hello World!","W");
echo($str);

echo "<br>";

//The empty space will add the backslash between the numbers
//Since they have spaces between them 1 space 2 space 3 space 4
//Got it? :)
$str2 = addcslashes("1 2 3 4", " "); 
echo $str2;