<?php

    //PHP addslashes() Function
    //Add a backslash in front of each double quote ("):

    $str = addslashes('What does "yolo" mean?');
    echo($str);
    echo "<br>";
    //Definition and Usage
    //The addslashes() function returns a string with 
    //backslashes in front of predefined characters.
    
    //The predefined characters are:
    //single quote (')
    //double quote (")
    //backslash (\)
    //NULL

    //addslashes(string) 

    //Example:

    $str = "Who's Peter Griffin?";
    echo $str . " This is not safe in a database query.<br>";
    echo addslashes($str) . " This is safe in a database query.";

