<?php
    //chop() Function
    //Remove characters from the right end of a string:

    /*charlist	Optional. Specifies which characters to remove from the string. 
    The following characters are removed if the charlist parameter is empty:
    "\0" - NULL
    "\t" - tab
    "\n" - new line
    "\x0B" - vertical tab
    "\r" - carriage return
    " " - ordinary white space*/

    //Example 1
    $str = "Hello World!";
    echo $str . "<br>";
    echo chop($str,"World!");
    echo "<br>";

    //Example 2
    $str2 = "Are you serious?!";
    echo chop($str2,"?!");


?>