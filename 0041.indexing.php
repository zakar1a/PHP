<?php

$name = "Mike";

echo "We'll use indexing to extract data from a variable, this example shows how I extracted letters from a variable starting with [0],[1] etc... based on variable text order" . "<br>" . "<br>";
echo "name is Mike" . "<br>" . "<br>" . "<br>";



echo $name[0] . "<br>";
echo $name[1] . "<br>";
echo $name[2] . "<br>";
echo $name[3] . "<br>" . "<br>";



echo "Indexing in an Array" . "<br>" . "<br>";

$my_array = ['Mark','Dave','Mike','Jennifer','Louis','Jimi'];
echo $my_array[1];
echo "&";
echo $my_array[0];





?>