<?php

//When you want to combine strings in php, you can do it by using concatentation operators

//Example 1

$first_name = "Zakaria";
$last_name = " Temli";
echo "This is " . $first_name . $last_name . "<br>";


//Example 2

$var1 = "This is";
$var1.= " the start";
echo $var1;

echo "<br>";

//Example 3
echo "This is ";
$name1 = "John";
$name1.= " Doe";
echo $name1;

?>