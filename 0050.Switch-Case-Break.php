<?php

    //Exercise switch/case/break

    //your assignment in this PHP exercise is to rewrite the poem exercise using 
    //a select field for the user input and the switch statement to process the response.

    //the poem:

    //Laugh on Monday, laugh for danger.
    //Laugh on Tuesday, kiss a stranger.
    //Laugh on Wednesday, laugh for a letter.
    //Laugh on Thursday, something better.
    //Laugh on Friday, laugh for sorrow.
    //Laugh on Saturday, joy tomorrow. 

    $br =   "<br>";                             //Return to line
    $current_day    =    date("l");             //Whats the day today in a variable, right? :)
    echo "Today is " . $current_day . $br;

    switch ($current_day) {
        case 'Monday':
            echo "Laugh on Monday, laugh for danger.";
            break;
        case 'Tuesday':
            echo "Laugh on Tuesday, kiss a stranger.";
            break;
        case 'wednesday':
            echo "Laugh on Wednesday, laugh for a letter.";
            break;
        case 'Thursday':
            echo "Laugh on Thursday, something better.";
            break;
        case 'Friday':
            echo "Laugh on Friday, laugh for sorrow.";
            break;
        case 'Saturday':
            echo "Laugh on Saturday, joy tomorrow.";
            break;
        
        default:
            echo " I can't figure out what's the day today lol";
            break;
    }

    