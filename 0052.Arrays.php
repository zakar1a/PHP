<?php

$br = "<br>";
// Variables are limited to store one value at a time. When we need to store multiple values we use Arrays. An array stores values and associates them with an index number, the first item you store in an array is stored at index[0] with each consecutive value incrementing by 1. Check the following Example

//Example 1

$arrayName = array('Summer','Fall','Winter');
print $arrayName[2] . $br; // This will print Winter
print $arrayName[0] . $br; // This will print Summer

//Example 2

$array1 = $arrayName = array('Fall','Winter', 'Spring','Summer');
//print_r returns human readable information about the target variable.
print_r($array1) . $br; // This will print the array with their index : Array ( [0] => Fall [1] => Winter [2] => Spring [3] => Summer )


//Example 3
$flavors = ["Vanilla","Choclate"];
$flavors[] = "Strawberries";
$flavors[] = "Banana";
//if you would like to add more you can use array_push
array_push($flavors, "Caramel","Butterscotch","Mango");
//If you would like to get an element to be the first in order you can use array_unshift
array_unshift($flavors, "Lemon");
// Lemon will have an index of [0]
print_r($flavors); //Output: Array ( [0] => Lemon [1] => Vanilla [2] => Choclate [3] => Strawberries [4] => Banana [5] => Caramel [6] => Butterscotch [7] => Mango )


//To remove the last element in the array we use array_pop($Data) => This will remove Data3
$Data = ["Data1","Data2","Data3"];
array_pop($Data);
print_r($Data); // This will exclude last element [Data3]
//To remove the first element of the array we use array_shift($Test)
$Test = ["1","2","3","4"];
array_shift($Test);
print_r($Test); // This will result first element [1]








?>