<?php

        $br = "<br>";
        $authors = array( "Mark", "David", "John", "Adam" );
        $authorsSlice = array_slice( $authors, 1,2 );
        // Displays Array ( [0] => David [1] => John ) 
        print_r( $authorsSlice );

        echo $br;
        //Associative Arrays

        $myBook = array( "title" => "The Grapes of Wrath",
        "author" => "John Steinbeck",
        "pubYear" => 1939 );
        $myBookSlice = array_slice( $myBook, 1, 2 );
        // Displays “Array ( [author] = > John Steinbeck [pubYear] = > 1939 )”;
        print_r( $myBookSlice );
        echo $br;

        //To keep indices order, you need a third argument => true

        $authors = array( "Steinbeck", "Kafka", "Tolkien", "Dickens" );
        // Displays “Array ( [0] = > Tolkien [1] = > Dickens )”;
        print_r( array_slice( $authors, 2, 2 ) );
        echo $br;
        // Displays “Array ( [2] = > Tolkien [3] = > Dickens )”;
        print_r( array_slice( $authors, 2, 2, true ) );