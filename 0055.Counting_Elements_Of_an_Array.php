<?php
        $br = "<br>";
        $authors = array( "Steinbeck", "Kafka", "Tolkien", "Dickens" );
        $myBook = array( "title" => "The Grapes of Wrath",
        "author" => "John Steinbeck",
        "pubYear" => 1939 );
        echo count( $authors ) . $br; // Displays “4”
        echo count( $myBook ) . $br; // Displays “3
        echo $br;

        $authors = array( "Steinbeck", "Kafka", "Tolkien", "Dickens" );
        $lastIndex = count( $authors ) - 1;
        echo $authors[$lastIndex]; // Displays “Dickens”