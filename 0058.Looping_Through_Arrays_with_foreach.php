<?php
    //Syntax:
    //  foreach ( $array as $value ) {
    //      (do something with $value here)
    //      }
    //      (rest of script here)
    $br = "<br>";

    // Loop Through Values

    $authors = array( "Mark", "John", "Zack", "Tim" );
        foreach ( $authors as $val ) {
        echo $val . $br;
        }

    // Loop Through Keys and Values

    //foreach ( $array as $key = > $value ) {
    //      (do something with $key and/or $value here
    //      }
    //      (rest of script here)

    echo $br;

    $myBook = array( "title" => "The Grapes of Wrath",
        "author" => "John Steinbeck",
        "pubYear" => 1939 );
        foreach ( $myBook as $key => $value ) {
        echo $key . $br;
        echo $value . $br;
        }

    echo $br;

    // Altering Array Values with foreach

    // When using foreach, the values you work with inside the loop are copies of the values in the array
    // itself. This means that if you change the value given to you by foreach, you ’ re not affecting the
    // corresponding value in the original array. The following example code illustrates this:

    $authors = array( "Steinbeck", "Kafka", "Tolkien", "Dickens" );
    // Displays “Steinbeck Kafka Hardy Dickens”;
    foreach ( $authors as $val ) {
    if ( $val == "Tolkien" ) $val = "Hardy";
    echo $val . " ";
    }
    echo $br;
    // Displays “Array ( [0] = > Steinbeck [1] = > Kafka [2] = > Tolkien [3] = > Dickens )”
    print_r ( $authors );


    //if you do want to modify the array values themselves, you can get foreach() to return a
    //reference to the value in the array, rather than a copy. This means that the variable within the loop points
    //to the value in the original array element, allowing you to change the element ’ s value simply by
    //changing the variable’ s value.
    //To work with references to the array elements rather than copies of the values, simply add a &
    //(ampersand) symbol before the variable name within the foreach statement:
        echo $br;

    $authors = array( "Steinbeck", "Kafka", "Tolkien", "Dickens" );
        // Displays “Steinbeck Kafka Hardy Dickens”;
        foreach ( $authors as &$val ) {
        if ( $val == "Tolkien" ) $val = "Hardy";}
        unset ($val);
        echo $br;
        // Displays “Array ( [0] = > Steinbeck [1] = > Kafka [2] = > Tolkien [3] = > Dickens )”
        print_r ( $authors );
    