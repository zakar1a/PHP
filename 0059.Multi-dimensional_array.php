<?php

    //Also known as Nested Arrays
    $br = "<br>";
    $myBooks = array(
        array(
        "title" => "The Grapes of Wrath",
        "author" => "John Steinbeck",
        "pubYear" => 1939
        ),
        array(
        "title" => "The Trial",
        "author" => "Franz Kafka",
        "pubYear" => 1925
        ),
        array(
        "title" => "The Hobbit",
        "author" => "J. R. R. Tolkien",
        "pubYear" => 1937
        ),
        array(
        "title" => "A Tale of Two Cities",
        "author" => "Charles Dickens",
        "pubYear" => 1859
        ),
        );

        echo  "<pre>";
        print_r ( $myBooks );
        echo "</pre>";
        echo $br;
        
        //Accessing Elements of Multidimensional Arrays

        // Displays “Array ( [title] = > The Trial [author] = > Franz Kafka [pubYear] = > 1925 )”;
        echo "<pre>";
        print_r( $myBooks[1] );
        // Displays “The Trial”
        echo $br . $myBooks[1]["title"] . $br;
        // Displays “1859”
        echo $myBooks[3]["pubYear"] . $br;
        echo "</pre>";

        echo $br;
        echo $br;

        //Looping Through Multidimensional Arrays
        //The following example uses two nested foreach loops to loop through the array

        $my_Favourite_Books = array(
            array(
                "title" => "The Alchemist",
                "author" => "Paulo Coelho",
                "PubYear" => "1990"
            ),
            array(
                "title" => "Portobello Belle",
                "author" => "Paulo Coelho",
                "PubYear" => "2000"
            ),
            array (
                "title" => "11 Minutes",
                "author" => "Paulo Coelho",
                "PubYear" => "2001"
            ),
            array(
                "title" => "The Miserable",
                "author" => "Victor Hugo",
                "PubYear" => "1991"
            ),
            array(
                "title" => "The Zahir",
                "author" => "Paulo Coelho",
                "PubYear" => "2002"
            ),
        );
        echo "<pre>";
        print_r ($my_Favourite_Books);
        echo "</pre>";
        echo $br;
        echo $br;

        $BookNum = 0;

        foreach($my_Favourite_Books as $book){
            $BookNum++;
            echo "<h2>Book #$BookNum:</h2>";
            echo "<dl>";
        foreach($book as $key => $value){
            echo "<dt>$key</dt><dd>$value</dd>";
        }
            echo "</dl>";
        }