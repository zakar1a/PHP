<?php

    //When it comes to sorting arrays, PHP provides no less than twelve functions 
    //that you can use to sort an array. The more common ones are:

    //1. sort() and rsort()– – For sorting indexed arrays
    //2. asort() and arsort()– – For sorting associative arrays
    //3. ksort() and krsort()– – For sorting associative arrays by key rather than by value
    //4. array_multisort()– – A powerful function that can sort multiple arrays at once, or multidimensional arrays

    // 1. Sorting Indexed Arrays with sort() and rsort()

    //The simplest of the array sorting functions are sort() and rsort(). sort() sorts the values of the
    //array in ascending order (alphabetically for letters, numerically for numbers, letters before numbers),
    //and rsort() sorts the values in descending order. To use either function, simply pass it the array to be
    //sorted. The function then sorts the array. As with all the sorting functions covered in this chapter, the
    //function returns true if it managed to sort the array or false if there was a problem.
    //Here ’ s an example that sorts a list of authors alphabetically in ascending order, and then in
    //descending order:

    $br = "<br>";
    $pre = "<pre>";
    $pree = "</pre>";
    echo "This is the original Array";
    $letters = array("g", "a", "h", "c", "b", "i", "f", "e", "d", "k", "j");
    echo $pre;
    print_r ($letters);
    echo $pree;

    echo "Sorting the array by order using sort";

    echo $pre;
    sort( $letters );
    print_r( $letters );
    echo $pree;    

    echo $br;

    echo "Sorting the array by order in reverse using rsort";

    echo $pre;
    rsort($letters);
    print_r($letters);
    echo $pree;
    
    echo $br;

    echo "Sorting the array";
    echo $pre;
    sort($letters);
    print_r($letters);
    echo $pree;

    
    $var = array( "title" => "Walk Of life",
                        "Band" => "dIRE sTRAITS",
                        "ReleaseYear" => "1980");

    echo "Showing original array";
    echo $pre;
    print_r ($var);
    echo $pree;
    echo $br;
    
    echo "preserve the association between each element’s key and its value using asort";

    echo $br;
    echo $pre;
    asort ($var);
    print_r($var);
    echo $pree;

    echo $br;
    echo $pre;
    echo "using arsort";
    echo $br;
    arsort ($var);
    print_r ($var);
    echo $pree;

    //Sorting Associative Array Keys with ksort() and krsort()
    //ksort() and krsort() behave in much the same way as asort() and arsort(), in that they sort
    //arrays in ascending and descending order, respectively, preserving the associations between keys and
    //values. The only difference is that, whereas asort() and arsort() sort elements by value, ksort()
    //and krsort() sort the elements by their keys:

    echo $br;
    echo $br;
    echo $pre;
    echo "Using ksort";
    echo $br;
    ksort ($var);
    print_r ($var);
    echo $pree;


    echo $br;
    echo $br;
    echo $pre;
    echo "Using krsort";
    echo $br;
    krsort ($var);
    print_r ($var);
    echo $pree;