<?php
    $pre = "<pre>";
    $pree = "</pre>";
    $br = "<br>";
    //array_multisort() lets you sort multiple related arrays at the same time, preserving the relationship
    //between the arrays. To use it, simply pass in a list of all the arrays you want to sort:
    //array_multisort( $array1, $array2, ... );

    $authors = array("Paulo Coelho", "Victor Hugo", "Maya Banks");
    $Titles = array("The Alchemist", "The Miserable", "The Surrender Trilogy");
    $Year_Of_Publication = array("2001","1990","2010");

    array_multisort ($Titles,$authors,$Year_Of_Publication);

    //echo $pre;
    print_r ($authors);
    //echo $pree;

    echo $br;
    print_r($Titles);
    echo $br;
    print_r($Year_Of_Publication);