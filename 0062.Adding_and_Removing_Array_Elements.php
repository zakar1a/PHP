<?php
    $br = "<br>";
    $pre = "<pre>";
    $pree = "</pre>";
    //This syntax is fine for simple scenarios. However, if you need something more powerful, PHP features
    //five useful functions that you can use to add and remove elements:

    //array_unshift()– – Adds one or more new elements to the start of an array
    //array_shift()– – Removes the first element from the start of an array
    //array_push() — Adds one or more new elements to the end of an array
    //array_pop() — Removes the last element from the end of an array
    //array_splice() — Removes element(s) from and/or adds element(s) to any point in an array

    //Adding and Removing Elements at the Start and End
    //You can use array_unshift() to insert an element or elements at the start of an array

    $bands = array("dIRE sTRAITS","Scorpions","ACDC", "Pink Floyd", "Beatles");

    //Let's add other bands without touching original array
    echo array_unshift ($bands, "Guns&Roses", "Metalica");
    echo $br;
    echo $pre;
    print_r ($bands);
    echo $pree;


    //Array Shift remove the first element out of the array and display its value not its Key

    $mybooks = array("title" => "The Alchemist",
                     "author" => "Paulo Cohelho",
                     "PubYear" => "2000"
    );
    echo "This is the original array";
    echo $pre;
    print_r($mybooks);
    echo $pree;
    echo $br;

    echo "Let's use now array_Shift";
    echo $pre;
    echo array_shift($mybooks);
    echo $pree;
    echo $br;
    

    //Using Array_Push
    $mybooks = array("TheAlechemist","TheZahir","11 Minutes");

    echo $pre;
    echo array_push($mybooks, "TEST1", "TEST2");
    print_r($mybooks);
    echo $pree;


    /////////////////////////////////////////////////////////////////////

    $mybands = array("DireStraits","PinkFloyd","ACDC","Beatles");
    $my_other_bands = array("Scorpions","Metallica");
    echo array_push($mybands,$my_other_bands);
    print $pre;
    print_r($mybands);
    print $pree;

    /////////////////////////////////////////////////////////////////////

    $mybooks = array("title" => "The Alchemist",
                     "author" => "Paulo Cohelho",
                     "PubYear" => "2000"
    );

    echo array_pop($mybooks); //Array_pop will retrieve the last value of the array - Display 2000
    echo $pre;
    print_r($mybooks);
    echo $pree;

    ////////////////////////////////////////////////////////////////////

    //Array Splice will add elements in the middle

    $fav_bands = array("DireStraits","Metallica","Beatles");
    array_splice($fav_bands, 1, 0, array("other_band" => "Scorpions"));
    echo $pre;
    print_r($fav_bands);
    echo $pree;

    ////////////////////////////////////////////////////////////////////

    //Merging arrays together
    $best_bands = array("DireStraits","Metallica","Beatles");
    $my_other_bands = array("Scorpions","ACDC","Pink Floyd");
    echo $pre;
    print_r(array_merge($best_bands,$my_other_bands));
    echo $pree;

    //Another Example:

    $My_ideas = array("Title" => "Idea 1",
                      "Proposed By" => "Zakaria",
                      "Date" => "June 2018"  
                        );
        $My_ideas = array_merge($My_ideas, array("Team" => "The Coders"));                
    echo $pre;
        print_r($My_ideas);
    echo $pree;

    //You can also use array_merge() to reindex a single numerically indexed array, simply by passing the
    //array. This is useful if you want to ensure that all the elements of an indexed array are consecutively
    //indexed

    $test_array = array (77 => "Display 0", 36 => "Display 1", 99 => "Display 2");
    echo $pre;
    print_r(array_merge($test_array));
    echo $pree;


    ///////////////////////////////////////////////////////////////////

    //Converting Arrays and Strings

    //First case : Using Explode to convert a string to an array

    $fruitstring = "Oranges,Bananas,Apples";
    $fruitsarray = explode(",", $fruitstring,3);
    echo $pre;
    print_r ($fruitsarray);           
    echo $pree;    

    //Second case: Using Implode to convert an array to a string

    $fruits_array = array("Oranges","Bananas","Apples");
    $fruitstring = implode(",", $fruits_array);
    echo $pre;
    echo $fruitstring;
    echo $pree;

    ////////////////////////////////////////////////////////////////////

    //Converting an Array to a List of Variables

    //Example:

    $my_fav_rock_band = array("DireStraits","Brothers In Arms","1988");

    $Band_name = $my_fav_rock_band[0];
    $Album_name = $my_fav_rock_band[1];
    $Release_Year = $my_fav_rock_band[2];

    echo $pre;
    echo $Band_name . $br;
    echo $Album_name . $br;
    echo $Release_Year . $br;
    echo $pree;

    $my_fav_rock_band = array("DireStraits","Brothers In Arms","1988");
    list($Band_name, $Album_name, $Release_Year) = $my_fav_rock_band;
    echo $Band_name . $br;
    echo $Album_name . $br;
    echo $Release_Year . $br;

    //Example

    $myBook = array( "title" => "The Grapes of Wrath",
                    "author" => "John Steinbeck",
                    "pubYear" => 1939 );
    while ( list( $key, $value ) = each( $myBook ) ) {
    echo "<dt> $key </dt>";
    echo "<dd> $value </dd>";
    }
