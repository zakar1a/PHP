<?php

function helloWithStyle( $font, $size ) {
    echo "<p style=\"font-family: $font; font-size: {$size}em;\">Hello, world!</p>";
    }
    helloWithStyle( "Helvetica", 2 );
    helloWithStyle( "Times", 3 );
    helloWithStyle( "Courier", 1.5 );
    


    //Optional Paramters and Default values

    //With previous example, we specified each size argument with a different size
    //if we would like to make it 1.5 for all the sizes
    //we add another optional parameter

    //syntax:
    //function myFunc( $parameterName=defaultValue ) {
    //    // (do stuff here)
    //    }

    //Example
    function helloWithStyl( $font, $size=1.5 ) {
        echo "<p style=\"font-family: $font; font-size: {$size}em;\">Hello, world!</p>";
        }
        helloWithStyl( "Helvetica", 4);
        helloWithStyl( "Times");
        helloWithStyl( "Courier");



