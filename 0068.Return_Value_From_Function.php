<?php
        $br = "<br>";
        //To get your function to return a value, you use — you guessed it — PHP ’ s return statement:
        //Syntax

        //function myFunc() {
        //  (do stuff here)
        //return value;
        //  }

        function makeBold( $text ) {
            return "<b>$text</b>";
            }
            $normalText = "This is normal text.";
            $boldText = makeBold( "This is bold text.");
            echo "<p>$normalText</p>";
            echo "<p>$boldText</p>";


        //This simply exits the function at that point, and returns control to the calling code. 
        //This is useful if you simply want a function to stop what it ’ s doing, 
        //without necessarily returning a value.    
        //Syntax:    

        // function myFunc() {
        // (do stuff here)
        //    return;
        //    }    


        //Understanding Function Scope

        //Example

        function helloWithVariables() {
            $hello = "Hello, ";
            $world = "world!";
            return $hello . $world;
            }
        echo helloWithVariables(); // Displays “Hello, world!” 


        //NOTE: variables created within a function are NOT accessible outside the function

        //Example:

        function helloWithVariables2() {
            $hello = "Hello, ";
            $world = "world!";
            return $hello . $world;
            }
            echo helloWithVariables2() . $br . $br;
            //echo "The value of \$hello is: '$hello'"; //Remove comments to see the effect
            //echo "The value of \$world is: '$world'"; //Remove comments to see the effect
            //This will display:
            //The value of $hello is: ''
            //The value of $world is: ''


        //Another point: variables created inside a function do not clash with variables created outside the function
        //Example:
        
        function describeMyDog() {
            $br = "<br>"; //Even I declared it in the begining of file, it wasn't recognized, must be declared inside function
            $color = "brown";
            echo "My dog is $color" . $br;
            }
            // Define my cat’s color
            $color = "black";
            // Display info about my dog and cat
            describeMyDog();
            echo "My cat is $color" . $br;