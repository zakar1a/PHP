<?php

    $a_global_variable = "This is a Global Text";

    function my_global(){
        global $a_global_variable;
        echo "$a_global_variable";
    }

    my_global();

    
    //Another Example:

    function setup() {
        global $myGlobal;
        $myGlobal = "Hello there!";
        }

    function hello() {
        global $myGlobal;
        echo "$myGlobal";
        }

    setup();
    echo "<br>";
    hello(); // Displays “Hello there!”

    //You can declare multiple global var
    //Syntax

    //function myFunction() {
    //    global $oneGlobal, $anotherGlobal;
    //    }