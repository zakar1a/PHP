<?php
    //sometimes it's useful to create a local variable that has a somewhat longer lifespan. Static
    //variables let you do just this. These types of variables are still local to a function, in the sense that they can
    //be accessed only within the function's code. However, unlike local variables, which disappear when a
    //function exits, static variables remember their values from one function call to the next


    function nextNumber() {
        static $counter = 0;
        return ++$counter;
        }
        echo "I’ve counted to: " . nextNumber() . "<br/>";
        echo "I’ve counted to: " . nextNumber() . "<br/>";
        echo "I’ve counted to: " . nextNumber() . "<br/>";