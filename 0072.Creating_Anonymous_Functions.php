<?php

    //You might want to create anonymous functions for two reasons:
    //To create functions dynamically — You can customize the code within an anonymous function
    //at the time that you create it. Although you'll rarely need to do this, it can make your code very
    //flexible in certain specific situations

    //To create short - term, disposable functions — Commonly, you do this when you work with built- in
    //functions that require you to create a callback or event handler function to work with. Examples
    //include xml_set_element_handler(), and array functions such as array_walk(), which lets you apply a 
    //function to each value in an array, and usort(), which sorts an array's elements according to a comparison 
    //function that you create yourself

    //Syntax
    //$myFunction = create_function( ‘$param1, $param2’, ‘function code here;’ );

    $plus_mode = "+";
    $minus_mode = "-";
    $processNumbers_plus = create_function( '$a, $b', "return \$a $plus_mode \$b;" );
    $processNumbers_minus = create_function('$a, $b', "return \$a $minus_mode \$b;");
    echo $processNumbers_plus( 2, 3 ); // Displays “5"
    echo "<br>";
    echo $processNumbers_minus(100,50); // Displays 50