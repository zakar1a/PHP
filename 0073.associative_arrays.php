<?php

//These types of arrays use named keys and associates them with values
//Examples

$ary = array(
		"eyes" => "blue",
		"age" => 34,
		"fav_movie" => "Harry Potter",
		"fav_animal" => "cat"
	);

print_r($ary); //To be all Data
print $ary["age"]; //To print specific data


?>