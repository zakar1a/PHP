<?php
    //Creating our first class:

    class  Car {
        public $color;
        public $manufacturer;
    }

    $bentley = new Car();
    $bentley->color = "red";
    $bentley->manufacturer = "Bentley";
    
    $mustang = new Car();
    $mustang->color = "Black";
    $mustang->manufacturer = "Ford";

    echo "<h2>Some properties:</h2>";
    echo "This car color is : " .$bentley->color . "<br>";
    echo "This car manufacturer is:" . $bentley->manufacturer . "<br>";

    echo "This car color is : " .$mustang->color . "<br>";
    echo "This car manufacturer is:" . $mustang->manufacturer . "<br>";
  
    echo "<pre>";
    print_r($bentley);
    echo "</pre>";
    echo "<br>";
    echo "<pre>";
    print_r($mustang);
    echo "</pre>";