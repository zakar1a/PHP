<?php

    //Example

    function resetCounter( $c ) {
        $c = 0;
        }
        $counter = 0;
        $counter++;
        $counter++;
        $counter++;
        echo "$counter <br/>"; // Displays “3”
        resetCounter( $counter );
        echo "$counter <br/>"; // Displays “3”

        //A reference is a bit like a shortcut or alias to a file on your hard drive. 
        //When you create a reference to a PHP variable, you now have two ways to read 
        //or change the variable ’ s contents — you can use the variable name, or you 
        //can use the reference. Here ’ s a simple example that creates a reference to a variable:


        $myVar = 123;
        $myRef = &$myVar;
        $myRef++;
        echo $myRef . "<br/>"; // Displays “124”
        echo $myVar . "<br/>"; // Displays “124"

        //Passing References to Your Own Functions

        //To get a function to accept an argument as a reference rather than a value, put an ampersand (&) before
        //the parameter name within the function definition:
        //Syntax

        //function myFunc( &$aReference ){
        // (do stuff with $aReference)
        //  }
        
        //Adding the ampersand before the $c causes the $c parameter to be a reference to the passed argument
        //($counter in this example)
        //Example 

        function resetCounterr( &$c ) { //Let's make a function and put an argument with reference
            $c = 0; //Let's intialise the reference to 0
            }
            $counter = 0; //Initial Value is 0
            $counter++; //1
            $counter++; //2
            $counter++; //3
            $counter++; //4
            $counter++; //5
            echo "$counter <br/>"; // Displays “5”
            resetCounterr( $counter ); //Let's reset the counter to Zero
            echo "$counter <br/>"; // Displays "0"
            $counter++; // Let's count it to "1" 
            echo $counter; // Let's print "1"

            echo "<br>"; //Return to a new line

        //Returning References from Your Own Function
        
        //As well as passing variables by reference into functions, you can also get functions to return references,
        //rather than values. To do this, you place an ampersand before the function name in your function
        //definition. Then, when you return a variable with the return statement, you pass a reference to that
        //variable back to the calling code, rather than the variable’ s value:
        //Syntax:
        //function &myFunc(){
        // (do stuff)
        //return $var; // Returns a reference to $var
        //}

        //Example that shows return - by - reference in action:

        $myNumber = 5;
        function &getMyNumber() {
        global $myNumber;
        return $myNumber;
            }
        $numberRef = &getMyNumber();
        $numberRef++;
        echo "$myNumber = $myNumber <br/>"; // Displays “6”
        echo "$numberRef = $numberRef <br/>"; // Displays “6”