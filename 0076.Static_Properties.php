<?php

    //Syntax:

    //class MyClass {
    //    public static $myProperty;
    //    }

    //Static members of a class are independent of any particular object derived from that class. To access
    //a static property, you write the class name, followed by two colons (::), followed by the property name   
    //(preceded by a $ symbol):
    //MyClass::$myProperty = 123;

    class Car {
        public $color;
        public $manufacturer;
        static public $NumberSold = 123;
    }

    Car::$NumberSold++;
    echo Car::$NumberSold; //Display 124