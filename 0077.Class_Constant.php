<?php

    //Syntax

    //class MyClass {
    //    const MYCONST = 123;
    //    }
    //    As with normal constants, it's good practice to use all - uppercase letters for class constant names

    //Like static properties, you access class constants via the class name and the :: operator:
    //echo MyClass::MYCONST;

    //Example:

    class Car {
        const HATCHBACK = 1;
        const STATION_WAGON = 1;
        const SUV = 3;
        public $model;
        public $color;
        public $manufacturer;
        public $type;
    }


    $MyCar = new Car;
    $MyCar->model = "Dodge Caliber";
    $MyCar->color = "blue";
    $MyCar->manufacturer = "Chrysler";
    $MyCar->type = Car::HATCHBACK;

    echo "This $MyCar->model is a ";
        switch ( $MyCar->type ) {
            case Car::HATCHBACK:
              echo "hatchback";
            break;
            case Car::STATION_WAGON:
                echo "station wagon";
            break;
            case Car::SUV:
                echo "SUV";
            break;
        }    
