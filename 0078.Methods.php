<?php

    //To add a method to a class, use the public, private, or protected keyword, then the function
    //keyword, followed by the method name, followed by parentheses. You then include the method’ s code
    //within curly braces:

    //class MyClass {
    //    public function aMethod() {
    //     (do stuff here)
    //    }
    //}

    //Calling a method syntax:
    //$object->Method();

    //Example:
    
    class MyClass {
        public function DaHellow(){
            echo "Hello, World";
        }
    }

    $obj = new MyClass;
    $obj->DaHellow(); //Display "Hello, World"
    ////////////////////////////////////////////////////////////////////////////////

    //Adding Parameters and Returning Values

    //You add parameters and return values in much the same way as with functions. To add parameters,
    //specify the parameter names between the parentheses after the method's name:
    //public function aMethod( $param1, $param2 ) {
    // (do stuff here)
    //}

    //To return a value from a method — or to simply exit a method immediately — use the return keyword:
    //    public function aMethod( $param1, $param2 ) {
    //    // (do stuff here)
    //    return true;
    //    }
    /////////////////////////////////////////////////////////////////////////////////

    //Accessing Object Properties from Methods
    //To access an object ’ s property from within a method of the same object, you use the special variable
    //name $this, as follows:
    //$this-> property;

    //Example:
    echo "<br>";
    class Greetings {
        public $greeting = "Hello everyone";
        public function Hello(){
            echo $this->greeting;
        }
    }
    
    $myobj = new Greetings;
    $myobj->Hello();

    //Another Example:

    echo "<br>";

    class idea {
        public function my_idea() {
            return "I'm thinking then I exist";
        }
        public function zakaria(){
            echo $this->my_idea();
        }
    }
    $obj2 = new idea;
    $obj2->zakaria();

    
    