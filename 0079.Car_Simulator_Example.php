<?php

    class Car{
        public $color;
        public $model;
        public $manufacturer;
        private $_speed = 0;
    

    public function accelerate(){
        if ($this->_speed >= 100) return false;
        $this->_speed +=10;
        return true;
    }

    public function brake(){
        if($this->_speed <= 0) return false;
        $this->_speed -= 10;
        return true;
    }

    public function getSpeed(){
        return $this->_speed;
    }

}
    $mycar = new Car();
    $mycar->color = "Red";
    $mycar->manufacturer = "BMW";
    $mycar->model = "CI";
    
    echo "<p>I'm driving a $mycar->color $mycar->manufacturer $mycar->model</p>";

    echo "<br>";

    echo "Stepping on the Gas";

    echo "<br>";

    while ($mycar->accelerate()){
        echo "Current speed ..." .  $mycar->getSpeed() . "Km<br>"; 
    }

    echo "<br>";

    echo "Slowing The Speed Down";

    echo "<br>";

    while ($mycar->brake()){
        echo "Current Speed is " . $mycar->getSpeed() . "km<br>";
    }

    echo "<br>";
    echo "Stopped";