<?php

    //PHP lets you create static methods in much the same way as static properties. To make a method static,
    //add the static keyword before the function keyword in the method definition:
    //class MyClass {
    //    public static function staticMethod() {
    // (do stuff here)
    //}
    //}
    //To call a static method, write the class name, followed by two colons, followed by the method name and
    //the arguments (if any) in parentheses:
    //MyClass::staticMethod();

    //As with static properties, static methods are useful when you want to add some functionality that’ s
    //related to a class, but that doesn ’ t need to work with an actual object created from the class. Here’ s a
    //simple example:

    class Car {
        public static function calcMpg( $miles, $gallons ) {
            return ( $miles / $gallons );
                }
        }
        echo Car::calcMpg( 168, 6 ); // Displays “28


    //Using Self:: keyword
    //Example

    class MyCar {
        public static function calcMpg( $miles, $gallons ) {
            return ( $miles / $gallons );
        }
        public static function displayMpg( $miles, $gallons ) {
            echo "This car’s MPG is: " . self::calcMpg( $miles, $gallons );
                }
        }
        echo "<br>";
        echo MyCar::displayMpg( 168, 6 ); // Displays “This car’s MPG is: 28

        //If you need to access a static method or property, or a class constant, from within a method of the same
        //class, you can use the same syntax as you would outside the class:
    class MyClass {
        const MYCONST = 123;
        public static $staticVar = 456;
        public function myMethod() {
            echo "<br>";
            echo "MYCONST = " . MyClass::MYCONST . ", ";
            echo "\$staticVar = " . MyClass::$staticVar . "<br/>";
            }
        }
        $obj = new MyClass;
        $obj-> myMethod(); // Displays “MYCONST = 123, $staticVar = 456”