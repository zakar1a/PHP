<?php

    //PHP allows you to create three " magic ” methods that you can use to intercept property and method accesses:
    //__get() is called whenever the calling code attempts to read an invisible property of the object
    //__set() is called whenever the calling code attempts to write to an invisible property of the object
    //__call() is called whenever the calling code attempts to call an invisible method of the object

    //Overloading Property Accesses with __get() and __set()

    class Car {
        public function __get( $propertyName ) {
        echo "The value of '$propertyName' was requested <br/>";
        return "blue";
            }
        }
        $car = new Car;
        $x = $car-> color; // Displays "The value of ‘color’ was requested”
        echo "The car’s color is $x <br/>"; // Displays "The car’s color is blue”


    //public function __set( $propertyName, $propertyValue ) {
    // (do whatever needs to be done to set the property value)
    //  }

        //Example:

        
        class MyCar {
        public $manufacturer;
        public $model;
        public $color;
        private $_extraData = array();

        public function __get( $propertyName ) {
        if ( array_key_exists( $propertyName, $this->_extraData ) ) {
                return $this-> _extraData[$propertyName];
            } else {
            return null;
            }
        }

        public function __set( $propertyName, $propertyValue ) {
        $this->_extraData[$propertyName] = $propertyValue;
            }
        }

        $myCar = new MyCar();
        $myCar->manufacturer = "Volkswagen";
        $myCar->model = "Beetle";
        $myCar->color = "red";
        $myCar->engineSize = 1.8;
        $myCar->otherColors = array( "green", "blue", "purple" );
        $myCar->fuelType = "Diesel";    

        echo "<h2>Some properties:</h2>";
        echo "<p> My car's manufacturer is " . $myCar->manufacturer . " .</p>";
        echo "<p> My car's engine size is " . $myCar->engineSize . ".</p>";
        echo "<p> My car's fuel type is " . $myCar->fuelType . ".</p>";
        echo "<h2>The \$myCar Object:</h2> <pre>";
        print_r( $myCar );
        echo "</pre>";

        //Overloading Method Calls with __call()
        // __call() is very useful if you want to create a “ wrapper ” class that doesn ’ t contain much functionality
        //of its own, but instead hands off method calls to external functions or APIs for processing

        //Syntax:
        //public function __call( $methodName, $arguments ) {
        //    // (do stuff here)
        //    return $returnVal;
        //    }

        class CleverString {
            private $_theString = "";
            private static $_allowedFunctions = array( "strlen", "strtoupper", "strpos" );
            public function setString( $stringVal ) {
                $this->_theString = $stringVal;
            }

            public function getString() {
                return $this-> _theString;
            }

            public function __call( $methodName, $arguments ) {
                if ( in_array( $methodName, CleverString::$_allowedFunctions ) ) {
                    array_unshift( $arguments, $this->_theString );
                    //call_user_func_array expects the function name as the first argument, and
                    //the argument list — as an array — as the second argument
                        return call_user_func_array( $methodName, $arguments );
                            } else {
                                die ( "<p>Method 'CleverString::$methodName' doesn't exist</p>" );
                            }
                        }
                    }

            $myString = new CleverString;
            $myString->setString( "Hello!" );
            echo "<p> The string is: " . $myString->getString() . "</p>";
            echo "<p> The length of the string is: " . $myString->strlen() . "</p>";
            echo "<p> The string in uppercase letters is: " . $myString->strtoupper() . "</p>";
            echo "<p> The letter 'e' occurs at position: " . $myString->strpos( "e" ) . "</p>";
            $myString-> madeUpMethod();

           
           
            

           