<?php

     //__callStatic() works like __call(), except that it is called whenever an attempt is made to call an
            //invisible static method. For example:

            class MyClass {
                public static function __callStatic( $methodName, $arguments ) {
                    echo "Static method '$methodName' called with the arguments: <br/>";
                        foreach ( $arguments as $arg ) {
                            echo "$arg <br/>";
                             }
                         }
                 }
            MyClass::randomMethod( "apple", "peach", "strawberry" );

                    