<?php
    //__unset() is called when the calling code attempts to delete an invisible property with PHP’ s
            //unset() function. It shouldn ’ t return a value, but should do whatever is necessary to “ unset ” the
            //property (if applicable):

                class myClass {
                    public function __unset( $propertyName ) {
                        echo "Unsetting property '$propertyName' <br /> ";
                        }
                    }
                    $testObject = new myClass;
                    unset( $testObject-> banana ); // Displays “Unsetting property ‘banana’”
        