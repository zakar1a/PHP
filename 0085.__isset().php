<?php
     //__isset()
            
            //__isset() is called whenever the calling code attempts to call PHP's isset() function on an invisible
            //property. It takes one argument — the property name — and should return true if the property is
            //deemed to be “ set, ” and false otherwise:

            class MyClass {
                public function __isset( $propertyName ) {
                // All properties beginning with “test” are “set”
                    return ( substr( $propertyName, 0, 4 ) == "test" ) ? true : false;
                    }
                }
            echo "<br>";    

            $testObject = new MyClass;
            echo isset( $testObject->banana ) . "<br/>"; // Displays “” (false)
            echo isset( $testObject->testBanana ) . "<br/>"; // Displays “1” (true)    
