<?php

    //Overriding Methods in the Parent Class
    //What if you want to create a child class whose methods are different from the corresponding methods in
    //the parent class? For example, you might create a class called Fruit that contains methods such as
    //peel(), slice(), and eat(). This works for most fruit; however, grapes, for example, don ’ t need to be
    //peeled or sliced. So you might want your Grape object to behave somewhat differently to the generic
    //Fruit object if you try to peel or slice it.
    //PHP, like most object - oriented languages, lets you solve such problems by overriding a parent class’ s
    //method in the child class. To do this, simply create a method with the same name in the child class. Then,
    //when that method name is called for an object of the child class, the child class ’ s method is run instead of
    //the parent class’ s method:

    //class ParentClass {
    //public function someMethod() {
    // (do stuff here)
    //}
    //}

    //class ChildClass extends ParentClass {
    //public function someMethod() {
    // This method is called instead for ChildClass objects
    //}
    //}
    
    //$parentObj = new ParentClass;
    //$parentObj- >someMethod(); // Calls ParentClass::someMethod()
    //$childObj = new ChildClass;
    //$childObj- >someMethod(); // Calls ChildClass::someMethod()

    //Example:

    class Fruit {
        public function peel() {
            echo "<p> I'm peeling the fruit... </p>";
        }

        public function slice() {
            echo "<p> I'm slicing the fruit... </p>";
        }

        public function eat() {
            echo "<p> I'm eating the fruit. Yummy! </p>";
        }

        public function consume() {
            $this-> peel();
            $this-> slice();
            $this-> eat();
            }
        }

        class Grape extends Fruit {

        public function peel() {
            echo "<p> No need to peel a grape! </p>";
        }

        public function slice() {
            echo "<p> No need to slice a grape! </p>";
        }
    }

    echo "<h2>Consuming an apple...</h2>";
    $apple = new Fruit;
    $apple-> consume();
    echo "<h2>Consuming a grape...</h2>";
    $grape = new Grape;
    $grape-> consume();