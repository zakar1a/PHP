<?php

    //Preserving the Functionality of the Parent Class

    //Occasionally you want to override the method of a parent class in your child class, but also use some of
    //the functionality that is in the parent class’s method. You can do this by calling the parent class’s
    //overridden method from within the child class’s method. To call an overridden method, you write
    //parent:: before the method name:
    //parent::someMethod();

    class Fruit {
        public function peel() {
            echo "<p> I'm peeling the fruit... </p>";
        }

        public function slice() {
            echo "<p> I'm slicing the fruit... </p>";
        }

        public function eat() {
            echo "<p> I'm eating the fruit. Yummy! </p>";
        }

        public function consume() {
            $this-> peel();
            $this-> slice();
            $this-> eat();
            }
        }

        class Grape extends Fruit {

        public function peel() {
            echo "<p> No need to peel a grape! </p>";
        }

        public function slice() {
            echo "<p> No need to slice a grape! </p>";
        }
    }

        //The New Code of Banana//
        class Banana extends Fruit {
            public function consume() {
            echo "<p> I’m breaking off a banana... </p>";
            parent::consume();
                }
            }
            $banana = new Banana;
            $banana-> consume();
        //End of Code//    

    echo "<h2>Consuming an apple...</h2>";
    $apple = new Fruit;
    $apple-> consume();
    echo "<h2>Consuming a grape...</h2>";
    $grape = new Grape;
    $grape-> consume();

