<?php

//We can create arrays of arrays.
//By doing we can better organize our data
//We can create arrays of arrays that store arrays.
//Sounds freaking huh? lol

//Example

$my_array = array(
	array(
		"name" => "Kim",
		"age" => 34,
		"gender" => "Female",
		),
	array(
		"name" => "Jack",
		"age" => 12,
		"Gender" => "Male",
		));
	print_r($my_array);

//To print a specific data of an array
//Example

	print $my_array[1]["age"]; // output is 12
?>