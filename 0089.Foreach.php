<?php

//We will use the foreach keyword to create an instruction
//that will be executed 'with each' element in the array
//Example:

$ary = array(2,3,4,5);
		foreach ($ary as &$number) {
			$number = $number * 2;
			echo "Result is $number" . "<br>";
		}

?>