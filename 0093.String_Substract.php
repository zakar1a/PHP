<?php
//To substract data out of a string
//Example1
	$var2 = "ABCDEFGHIJKLMN";
	$sub_var = substr($var2,0,1); //Result is A - 0,1 means 0+1 = 1 which means print only the first letter
	print $sub_var . "<br>";

//Example2
	$var3 = "Hello Everyone";
	$var_sub2 = substr($var3,0,2);
	//This will require php to start from zero and grab 2 letters which will be "He"
	print $var_sub2;

?>