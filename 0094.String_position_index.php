<?php

      //Finding a word in a string and get its position index
      $var = "Hello World";
      $var_sub = strpos($var, "World");
      //This will print the position index of the string by counting letters until the word World is reached
      //It will take 6 letters in addition to the space between Hello and World, Think about it :)
      //Result is 6
      print $var_sub;
