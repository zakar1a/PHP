<?php

//Example 1
for($counter = 1; $counter <=5; $counter++){
	print $counter . " times 3 is " . ($counter * 3) . "<br>";
}
//Result
//1 times 3 is 3
//2 times 3 is 6
//3 times 3 is 9
//4 times 3 is 12
//5 times 3 is 15


//Example 2

$fruit = array("Apples","Oranges","Bananas");
$length = count($fruit);
	for ($i=0 ; $i < $length ; $i++){
		echo $fruit[$i] . "<br>";
	}
//Result:
//Apples
//Oranges
//Bananas
?>