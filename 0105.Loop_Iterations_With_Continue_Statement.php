<?php

        //The following example counts from 1 to 10, but it misses out the number 4 
        //(which is considered unlucky in many Asian cultures):

        for ($i=1; $i < 10; $i++) { 
            if ($i == 4) continue;
            echo "Ive counted to: $i <br>";
        }

        echo "All Done!";


?>