<?php
    // While Excerise.
    $items  = 10;
    $br     = "<br>";
    
    while($items > 0){
        echo "Selling an item" . $br;
        $items--;
        // I added this condition to remove the s from item(s) when it reaches 1 or 0 to make it more
        // sense than the generic output. PHP is stupid at this point.
        if ($items == 1 || $items == 0) {                        
            echo "Done, There are now $items item left" . $br;   
        } else {                                               
        echo "Done, There are now $items items left" . $br;}
    }
        echo "We're out of items";