<?php
/**
* 
*/
class Person {
	private $_firstName;
	private $_lastName;
	private $_age;

	public function __construct($firstName, $lastName, $age){
		$this->_firstName = $firstName;
		$this->_lastName = $lastName;
		$this->_age = $age;
	}

	public function ShowMeTheDetails(){
		echo "$this->_firstName $this->_lastName $this->_age <br>";
	}

}

$p = new Person("Harry","Walt","32");
$p->ShowMeTheDetails();

?>