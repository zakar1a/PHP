<?php 
/*

Storing objects as Strings
PHP provides 2 functions to help with this:

Serialize + Unserialize.

*/

class Person{
public $age;
}

$harry = new Person;
$harry->age = 20;
$harryString = serialize($harry);
echo "Harry is now serialized in the following string: '$harryString'<br/>";
echo "Converting '$harryString' back to an object...<br>";
$obj = unserialize($harryString);
echo "Harry's age is: $obj->age<br>";